package com.shankar.noteit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth


class IntroActivity : AppCompatActivity() {

    var mFirebaseAuth = FirebaseAuth.getInstance()
    //lateinit var mFirebaseAuthStateListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Hiding actionbar and title bar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.hide()
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_intro)

//        mFirebaseAuthStateListener = FirebaseAuth.AuthStateListener {


    }

}

